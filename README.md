That it's a repository for the airports/countries applications deployment.
That approach leverages docker-compose && docker swarm.
Applications will be deployed in isolated and separated envs and access from the outside world only via balancer. 

Prerequisites:

-installed docker > 18.0

-initialized docker swarm

Steps:

1.`git clone https://andrii11@bitbucket.org/andrii11/task.git`

2.`cd <cloned repo directory>`

3.`./deploy.sh` - it will build all needed docker images and start deploy process

4.when deploy will finish simply run docker stack services test(default name for the stack) to see if everything was deployed successfully 

5.you can start test the application, for example with curl localhost/countries

Further:

-if you want to launch no-downtime update of your app you can run(for example with airports app) : `docker service update --image airports:1.1.0 test_airports`

-if you want to redeploy - just launch `./redeploy.sh`