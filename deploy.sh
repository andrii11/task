#!/bin/bash
cd airports-1.0.1/ && docker build -t=airports:1.0.1 .
cd ../airports-1.1.0/ && docker build -t=airports:1.1.0 .
cd ../countries-1.0.1/ && docker build -t=countries:1.0.1 .
cd ../ && docker stack deploy --compose-file=docker-compose.yml test
